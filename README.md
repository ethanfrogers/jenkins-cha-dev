# jenkins-cha-dev

The "where was this when I started using Jenkins" repo.

## Requirements

* Docker 1.12

## Anatomy

* `docker-compose.yml` - cluster configuration for Jenkins, a NodeJS build agent and a DinD (Docker in Docker) build agent
* `Dockerfile.node` - a NodeJS build agent configured with the Swarm client
* `plugins.txt` - all the plugins used for this demo (includes dependencies)
* `app/` - a simple NodeJS module complete with testing
* `app/Jenkinsfile` - the build script that defines each step required to test the module
* `jobs/jobs.groovy` - Job definitions to be executed via the Seed Job


## Getting Started

While most of the demo is already configured, you will still need to configure your Jenkins instance with a user and setup the Seed Job.

1. `docker-compose up jenkins` - this will launch jenkins in the foreground. Watch the logs for the initial admin password!
2. Visit localhost:8080 and wait for Jenkins to start
3. Once Jenkins is loaded you will be asked for the initial admin password.
4. Either install suggested plugins or select some to install (it shouldn't matter).
5. Create your new user
6. Navigate to your new user "Manage Jenkins > Manage Users > {your new user}" and grab your API token, we'll use this to connect our build agents.
7. Add the following to your `~/.bash_profile` or a `.env` file.

	```
	export SWARMUSER={your-user-name}
	export SWARMPW={your-api-token}
	```
8. `docker-compose up -d` will start the remaining build agents and they will connect automatically.


From here you're ready to start experimenting with Jenkins. If you'd like to experiment with the jobs included in this project, see the next section.

## Setting up the Seed Job

Once you have build agents connected, you're ready to start running jobs. You can create jobs manually or create a Seed Job that will create the two jobs defined in `jobs/jobs.groovy`. 

1. Click "New Item" on the left hand menu.
2. Name your job and select Freestyle Job
3. Under "Source Code Management" choose "Git" and enter the url for this repository (or your own if you've forked the repo).
4. Set "Branches to Build" to `master`.
5. Under "Build", add the "Process Job DSLs" build step and select "Look on Filesystem". 
6. For DSL Scripts, enter `jobs/jobs.groovy`.
8. Click Save and once redirected to the Build screen, click "Build Now"

If you click Jenkins in the top left most corner you should be taken back to your jobs list where you will see 3 jobs: your seed job, `jenkins-cha-dev` and `jenkins-chadev-docker-pipeline`. You can click either of the `jenkins-cha-dev` jobs and build them as well. If everything is configured correctly, they _should_ pass.


## Preconfigured Jobs

### jenkins-cha-dev

This job clones this repo, checksout `master`, installs NPM dependencies and runs the tests for the module. If tests pass, the JUnit test results and HTML code coverage report will be archived by Jenkins and made viewable on the jobs Build screen.

### jenkins-cha-dev-docker-pipeline

This job is the same as above, however it runs on a build agent labeled `docker`. Instead of running the build steps directly on the build agent, it creates a new Docker container from `node:latest` and runs the steps inside of that container. The goal of this jobs is to demonstrate that we can have very simple build agents that only run Docker to launch whatever kind of process we want or need.